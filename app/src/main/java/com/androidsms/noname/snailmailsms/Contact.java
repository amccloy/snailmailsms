package com.androidsms.noname.snailmailsms;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alex on 12/01/2016.
 */
public class Contact {


	public String name;
	public String number;

	public Contact(String jsons) {
		try {
			JSONObject json = new JSONObject(jsons);
			this.number = json.getString("number");
			this.name = json.getString("name");

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public Contact(){};

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name).append("\n");
		sb.append(number);
		return sb.toString();
	}

	public String toJson(){
		JSONObject json = new JSONObject();
		try {
			json.put("name", this.name);
			json.put("number", this.number);

			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return "ERROR";
		}

	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}
