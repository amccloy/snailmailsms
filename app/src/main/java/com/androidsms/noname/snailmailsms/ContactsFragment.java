package com.androidsms.noname.snailmailsms;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.provider.ContactsContract;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

/**
 * Created by alex on 12/01/2016.
 */
public class ContactsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,
		AdapterView.OnItemClickListener  {

	@SuppressLint("InlinedApi")
	private final static String[] FROM_COLUMNS = {
			Build.VERSION.SDK_INT
					>= Build.VERSION_CODES.HONEYCOMB ?
					ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
					ContactsContract.Contacts.DISPLAY_NAME
	};
	/*
	 * Defines an array that contains resource ids for the layout views
	 * that get the Cursor column contents. The id is pre-defined in
	 * the Android framework, so it is prefaced with "android.R.id"
	 */
	private final static int[] TO_IDS = {
			android.R.id.text1
	};
	// Define global mutable variables
	// Define a list_view object
	ListView mContactsList;
	// Define variables for the contact the user selects
	// The contact's _ID value
	long mContactId;
	// The contact's LOOKUP_KEY
	String mContactKey;
	// A content URI for the selected contact
	Uri mContactUri;
	// An adapter that binds the result Cursor to the list_view
	private SimpleCursorAdapter mCursorAdapter;

	// Empty public constructor, required by the system
	public ContactsFragment() {}

	// A UI Fragment must inflate its View
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Inflate the fragment layout
		return null;
		//return inflater.inflate(R.layout.contact_list_fragment,container, false);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

	}
}
